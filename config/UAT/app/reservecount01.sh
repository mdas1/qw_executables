#!/bin/bash
NOTIFYEMAIL=mithun.d.das@ericsson.com
FROM_EMAIL=no-reply-IP-UAT@somos.com
yday=$(date -d "-1 days" +"%Y-%m-%d")
cd /var/quickwin/logs
touch temp.txt
cat backend-$yday.* | grep -a "sendToIMSForSearchAndReserve responseMessage" |  awk -F':' '{ print $12}' | awk -F'=' '{ print $3}' >> temp.txt
cat backend-$yday.* | grep -a "IMSSpringService.oneClickActivateFromIms Response Message ::" | awk -F',' '{ print $5}' | awk -F'=' '{ print $2}' >> temp.txt
cat backend-$yday.* | grep -a "COMPLD" | grep -ae "reserveNumbers Return message from ims system :::::::::GRSP-NSR:" | awk -F'=' '{ print $3}' | awk -F':' '{ print $1}' >> temp.txt
cat backend-$yday.* | grep -a "IMSSpringService.oneClickActivateFromIms Response Message ::" | grep "COMPLD" | awk -F',' '{ print $5}' | awk -F'=' '{ print $2}' >> temp.txt
cat backend-$yday.* | grep -a "ImsCustomerRecordUpdateResponse.buildResponseMessage responseMsg from IMS:" | grep "COMPLD" | awk -F',' '{ print $5}' | awk -F'=' '{ print $2}' >> temp.txt

resporg=(`cat temp.txt |sort | uniq`)
rm temp.txt
if [[ ${#resporg[@]} -eq 0 ]]; then
        echo "There was no activity in App01 server on $yday" |  mail -s "Quickwin data count for Production App01 server on $yday" -r $FROM_EMAIL $NOTIFYEMAIL
else
for (( i=0; i<${#resporg[@]}; i++ ));
do
WSRcount=`cat backend-$yday.* |grep -a "COMPLD" | grep -ae "sendToIMSForSearchAndReserve responseMessage" | grep ${resporg[$i]} | awk -F':' '{ print $13}' | awk -F'=' '{sum+=$2} END {print sum+0}'`
Actcount=`cat backend-$yday.* | grep -a "IMSSpringService.oneClickActivateFromIms Response Message ::" | grep ${resporg[$i]} |grep -c "COMPLD,00"`
SRcount=$((WSRcount - Actcount))
Rcount=`cat backend-$yday.* |grep -a "COMPLD" | grep -e "reserveNumbers Return message from ims system :::::::::GRSP-NSR:" | grep ${resporg[$i]} | awk -F':' '{ print $20}' | awk -F'=' '{sum+=$2} END {print sum+0}'`
totalcount=$((SRcount + Rcount))
SRAcount=`cat backend-$yday.* | grep -a "IMSSpringService.oneClickActivateFromIms Response Message ::" | grep "COMPLD" | grep ${resporg[$i]} | grep -vc "COMPLD,00"`
timpltcount=`cat backend-$yday.* | grep -a "ImsCustomerRecordUpdateResponse.buildResponseMessage responseMsg from IMS:" | grep -e "COMPLD,00" -e "COMPLD,11" | grep -c ${resporg[$i]}`

echo "Resporg :" ${resporg[$i]} >>temp.txt
echo "***************" >>temp.txt
echo -e "Search & Reserve count is :"$SRcount >>temp.txt
echo -e "\nOnly Reserve count is :"$Rcount >>temp.txt
echo -e "\nTotal reserved number in App01 server is :"$totalcount >>temp.txt
echo -e "\nActivate Count is :"$Actcount >>temp.txt
echo -e "\nSearch and Reserved but not Activated :"$SRAcount >>temp.txt
echo -e "\nNumber of Template created :"$tmpltcount >>temp.txt
echo " " >>temp.txt
done
cat temp.txt | mail -s "Quickwin data count for Production App01 server on $yday" -r $FROM_EMAIL $NOTIFYEMAIL
rm temp.txt
fi


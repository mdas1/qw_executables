#!/bin/bash
NOTIFYEMAIL=mithun.d.das@ericsson.com,sarbik.basu@ericsson.com,chayanika.pal@ericsson.com,abhishek.dutta.gupta@ericsson.com
#NOTIFYEMAIL=mithun.d.das@ericsson.com
FROM_EMAIL=no-reply-IP-INT@somos.com
yday=$(date -d "-1 days" +"%Y-%m-%d")
fname=$(ls /var/quickwin/logs/webui.* | sort -u | tail -1)
cd /var/quickwin/logs
resporg=(`cat webui-$yday.* | grep "entered application landing page." | awk -F' ' '{ print $9}' |sort | uniq`)
for (( i=0; i<${#resporg[@]}; i++ ));
do
lcount=`cat webui-$yday.* | grep "entered application landing page." | grep -c ${resporg[$i]}`
echo "Resporg :" ${resporg[$i]} >>temp.txt
echo "***************" >>temp.txt
echo "Number of ${resporg[$i]} RespOrg users visited Quickwin Landing page :" $lcount >>temp.txt
echo "" >>temp.txt
done
cat temp.txt | mail -s "Quickwin Landing page data count for WEB02 in Integration" -r $FROM_EMAIL $NOTIFYEMAIL
rm temp.txt


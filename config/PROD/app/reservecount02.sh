#!/bin/bash
NOTIFYEMAIL=munnone@somos.com,mkimmel@somos.com,melliott@somos.com,mbhat@somos.com
FROM_EMAIL=no-reply-IP@somos.com
yday=$(date -d "-1 days" +"%Y-%m-%d")
#yday=backend
#fname=$(ls /var/quickwin/logs/backend.* | sort -u | tail -1)
cd /var/quickwin/logs
FailedActivateCount(){
	
	respOrg=$1
	DeniedCount=`cat backend-$yday.* | grep -a "IMSSpringService.oneClickActivateFromIms Response Message ::" | grep $respOrg | grep -c "DENIED"`
	bulkActivate=`cat backend-$yday.* | grep -aA2 'Invalid CR was created. So stopping the OCA for this activities' | grep -v "Invalid CR was created. So stopping the OCA for this activities" | grep "NumbersController.searchReserveActivate Number Response" | grep $respOrg | grep -o tfnNumbers= | grep -c tfnNumbers`
	SRActivate=`cat backend-$yday.* | grep -aA1 'Invalid CR was created. So stopping the OCA for this activities' | grep -v "Invalid CR was created. So stopping the OCA for this activities" | grep "NumbersController.oneClickActivate Response :" | grep $respOrg | grep -o tfnNumbers= | grep -c tfnNumbers`
 	
	numberlist=($(cat backend-$yday.* | grep -a "IMSSpringService.oneClickActivateFromIms Response Message ::" | grep $respOrg | grep "DENIED" | awk -F'"' '{ print $2}'))
	dupcount=0
        if [[ ${#numberlist[@]} -eq 0 ]]; then
	    echo "no data in numberlist"
        else
	    for (( j=0; j<${#numberlist[@]}; j++ ));
   		do
			count1=`cat backend-$yday.* | grep -a ${numberlist[$j]} | grep -a "IMSSpringService.oneClickActivateFromIms Response Message ::" | grep $respOrg | grep -c "COMPLD,00"`
        		dupcount=$((dupcount + count1))
   		done
	fi
	totalfailedcount=$((DeniedCount + bulkActivate + SRActivate))
	failedcount=$((totalfailedcount - dupcount))
	return $failedcount
	
}

if [ -f "temp.txt" ]; then
	rm temp.txt
	touch temp.txt
else
	touch temp.txt
fi

cat backend-$yday.* | grep -a "sendToIMSForSearchAndReserve responseMessage" |  awk -F':' '{ print $12}' | awk -F',' '{ print $2}' | awk -F'=' '{ print $2}' >> temp.txt
cat backend-$yday.* | grep -a "IMSSpringService.oneClickActivateFromIms Response Message ::" | awk -F',' '{ print $5}' | awk -F'=' '{ print $2}' >> temp.txt
cat backend-$yday.* | grep -a "COMPLD" | grep -ae "reserveNumbers Return message from ims system :::::::::GRSP-NSR:" | awk -F'=' '{ print $3}' | awk -F':' '{ print $1}' >> temp.txt
cat backend-$yday.* | grep -a "IMSSpringService.oneClickActivateFromIms Response Message ::" | grep "COMPLD" | awk -F',' '{ print $5}' | awk -F'=' '{ print $2}' >> temp.txt
cat backend-$yday.* | grep -a "IMSSpringService.oneClickActivateFromIms Response Message ::" | grep "DENIED" | awk -F',' '{ print $5}' | awk -F'=' '{ print $2}' >>temp.txt
cat backend-$yday.* | grep -aA2 'Invalid CR was created. So stopping the OCA for this activities' | grep -v "Invalid CR was created. So stopping the OCA for this activities" | grep "NumbersController.searchReserveActivate Number Response" |  awk -F'=' '{ print $6}' |  awk -F',' '{ print $1}' >> temp.txt
cat backend-$yday.* | grep -aA1 'Invalid CR was created. So stopping the OCA for this activities' | grep -v "Invalid CR was created. So stopping the OCA for this activities" | grep "NumbersController.oneClickActivate Response :" | awk -F'=' '{ print $6}' |  awk -F',' '{ print $1}' >> temp.txt

cat backend-$yday.* | grep -a "ImsCustomerRecordUpdateResponse.buildResponseMessage responseMsg from IMS:" | grep "COMPLD" | awk -F',' '{ print $5}' | awk -F'=' '{ print $2}' >> temp.txt



resporg=(`cat temp.txt |sort | uniq`)
rm temp.txt

Total_WSRcount=`cat backend-$yday.* |grep -a "COMPLD" | grep -ae "sendToIMSForSearchAndReserve responseMessage" | awk -F':' '{ print $13}' | awk -F'=' '{sum+=$2} END {print sum+0}'`
Total_Actcount=`cat backend-$yday.* | grep -a "IMSSpringService.oneClickActivateFromIms Response Message ::" |grep -c "COMPLD,00"`
Total_SRcount=$((Total_WSRcount - Total_Actcount))
Total_Rcount=`cat backend-$yday.* |grep -a "COMPLD" | grep -e "reserveNumbers Return message from ims system :::::::::GRSP-NSR:" | awk -F':' '{ print $20}' | awk -F'=' '{sum+=$2} END {print sum+0}'`
Total_totalcount=$((Total_SRcount + Total_Rcount))
FailedActivateCount Response
Total_SRAcount=$?
Total_tmpltcount=`cat backend-$yday.* | grep -a "ImsCustomerRecordUpdateResponse.buildResponseMessage responseMsg from IMS:" | grep -ce "COMPLD,00" -ce "COMPLD,11"`


if [[ ${#resporg[@]} -eq 0 ]]; then
        echo "There was no activity in app02 on $yday" |  mail -s "Quickwin data count for App02 in Production on $yday" -r $FROM_EMAIL $NOTIFYEMAIL
else
	echo "***************" >>temp.txt
	echo "SUMMARY REPORT"  >>temp.txt
	echo "***************" >>temp.txt
	echo -e "Total Number of Search & Reserve count :"$Total_SRcount >>temp.txt
	echo -e "\nTotal Number of Only Reserve count is :"$Total_Rcount >>temp.txt
	echo -e "\nTotal reserved number in App02 server :"$Total_totalcount >>temp.txt
	echo -e "\nTotal number of Activate Count :"$Total_Actcount >>temp.txt
	echo -e "\nSearch and Reserved but not Activated :"$Total_SRAcount >>temp.txt
	echo -e "\nTotal Number of Template created :"$Total_tmpltcount >>temp.txt
	echo "***************">>temp.txt
	echo " " >>temp.txt
	echo "******* REPORT BREAKUP *******" >>temp.txt
	echo "" >>temp.txt
	echo "" >>temp.txt

	for (( i=0; i<${#resporg[@]}; i++ ));
	  do
		WSRcount=`cat backend-$yday.* |grep -a "COMPLD" | grep -ae "sendToIMSForSearchAndReserve responseMessage" | grep ${resporg[$i]} | awk -F':' '{ print $13}' | awk -F'=' '{sum+=$2} END {print sum+0}'`
		Actcount=`cat backend-$yday.* | grep -a "IMSSpringService.oneClickActivateFromIms Response Message ::" | grep ${resporg[$i]} |grep -c "COMPLD,00"`
		SRcount=$((WSRcount - Actcount))
		Rcount=`cat backend-$yday.* |grep -a "COMPLD" | grep -e "reserveNumbers Return message from ims system :::::::::GRSP-NSR:" | grep ${resporg[$i]} | awk -F':' '{ print $20}' | awk -F'=' '{sum+=$2} END {print sum+0}'`
		totalcount=$((SRcount + Rcount))
		FailedActivateCount ${resporg[$i]}
		SRAcount=$?
		tmpltcount=`cat backend-$yday.* | grep -a "ImsCustomerRecordUpdateResponse.buildResponseMessage responseMsg from IMS:" | grep -e "COMPLD,00" -e "COMPLD,11" | grep -c ${resporg[$i]}`

		echo "Resporg :" ${resporg[$i]} >>temp.txt
		echo "***************" >>temp.txt
		echo -e "Search & Reserve count is :"$SRcount >>temp.txt
		echo -e "\nOnly Reserve count is :"$Rcount >>temp.txt
		echo -e "\nTotal reserved number in App02 server is :"$totalcount >>temp.txt
		echo -e "\nActivate Count is :"$Actcount >>temp.txt
		echo -e "\nSearch and Reserved but not Activated :"$SRAcount >>temp.txt
		echo -e "\nNumber of Template created :"$tmpltcount >>temp.txt
		echo " " >>temp.txt
	  done


	cat temp.txt | mail -s "Quickwin data count for App02 in Production on $yday" -r $FROM_EMAIL $NOTIFYEMAIL
	rm temp.txt
fi

